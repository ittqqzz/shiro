package com.tqz.shiro.service;

import com.tqz.shiro.dao.UserDao;
import com.tqz.shiro.entity.Auth;
import com.tqz.shiro.entity.Role;
import com.tqz.shiro.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void queryAllUsers() {
        List<User> users = userDao.queryAllUsers();
        System.out.println(users.toString());
    }

    @Test
    public void queryUserByUserName() {
        // 1.查询用户
        User user = userDao.queryUserByUsername("guest");
        // 2.查找用户对应的角色
        List<Role> roleList = new ArrayList<>();
        Integer role_id = userDao.queryRoleIdByUserId(user.getId());
        Role role = userDao.queryRoleByRoleId(role_id);
        // 3.查找角色对应的auth
        List<Integer> authIdList = userDao.queryAuthIdByRoleId(role.getId());
        List<Auth> authList = new ArrayList<>();
        for (Integer id:authIdList) {
            Auth auth = userDao.queryAuthByAuthId(id);
            authList.add(auth);
        }
        // 4.组装
        role.setAuthList(authList);
        roleList.add(role);
        user.setRoleList(roleList);
    }
}