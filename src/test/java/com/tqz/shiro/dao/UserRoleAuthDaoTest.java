package com.tqz.shiro.dao;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class UserRoleAuthDaoTest {

    @Autowired
    private UserRoleAuthDao userRoleAuthDao;

    @Test
    public void delRole() {
        userRoleAuthDao.delRole(1, 3);
    }
}