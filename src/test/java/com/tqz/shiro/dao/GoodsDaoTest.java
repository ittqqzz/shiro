package com.tqz.shiro.dao;

import com.tqz.shiro.entity.Goods;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;
@SpringBootTest
@RunWith(SpringRunner.class)
public class GoodsDaoTest {

    @Autowired
    private GoodsDao goodsDao;

    @Test
    public void saveOneGoods() {
        Goods goods = new Goods();
        goods.setName("香蕉");
        goods.setPrice(1.5);
        goods.setNum(1000);
        goodsDao.saveOneGoods(goods);
    }

    @Test
    public void delOneGoodsById() {
        goodsDao.delOneGoodsById(5);
    }

    @Test
    public void updateOneGoodsById() {
        Goods goods = goodsDao.queryOneGoodsByName("桌子");
        goods.setPrice(500.0);
        goodsDao.updateOneGoodsById(goods);
    }

    @Test
    public void queryOneGoodsByName() {
        Goods goods = goodsDao.queryOneGoodsByName("桌子");
        assertNotNull(goods);
    }

    @Test
    public void queryAllGoods() {
        List<Goods> goodsList = goodsDao.queryAllGoods();
        assertNotNull(goodsList);
    }
}