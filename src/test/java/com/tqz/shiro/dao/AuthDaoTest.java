package com.tqz.shiro.dao;

import com.tqz.shiro.entity.Auth;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;
@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthDaoTest {

    @Autowired
    private AuthDao authDao;

    @Test
    public void queryAuthList() {
        List<Auth> authList = authDao.queryAuthList();
        assertNotNull(authList);
    }

    @Test
    public void deleteOneAuthById() {
        authDao.deleteOneAuthById(5);
    }
}