package com.tqz.shiro.dao;

import com.tqz.shiro.entity.Auth;
import com.tqz.shiro.entity.Role;
import com.tqz.shiro.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.transform.Source;
import java.util.List;

import static org.junit.Assert.*;
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserDaoTest {

    @Autowired
    private UserDao userDao;

    @Test
    public void queryAllUsers() {
        List<User> userList = userDao.queryAllUsers();
        System.out.println("===========" + userList.toString());
    }

    @Test
    public void queryUserByUsername() {
        User user = userDao.queryUserByUsername("admin");
        System.out.println("============" + user.toString());
    }

    @Test
    public void queryRoleIdByUserId() {
        System.out.println("============" + userDao.queryRoleIdByUserId(1));
    }

    @Test
    public void queryRoleByRoleId() {
        Role role = userDao.queryRoleByRoleId(1);
        System.out.println("==========" + role.toString());
    }

    @Test
    public void queryAuthIdByRoleId() {
        System.out.println("============" + userDao.queryAuthIdByRoleId(1).toString());

    }

    @Test
    public void queryAuthByAuthId() {
        Auth auth = userDao.queryAuthByAuthId(1);
        System.out.println("===========" + auth.toString());
    }

    @Test
    public void saveOneUser() {
        User user = new User();
        user.setUsername("tqz");
        user.setPassword("0000");
        userDao.saveOneUser(user);
    }
}