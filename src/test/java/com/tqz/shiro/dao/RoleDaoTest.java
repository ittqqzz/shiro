package com.tqz.shiro.dao;

import com.tqz.shiro.entity.Role;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.*;
@SpringBootTest
@RunWith(SpringRunner.class)
public class RoleDaoTest {

    @Autowired
    private RoleDao roleDao;

    @Test
    public void getRoleList() {
        List<Role> roleList = roleDao.queryRoleList();
        assertNotNull(roleList);
    }

    @Test
    public void deleteOneRoleById() {
        roleDao.deleteOneRoleById(5);
    }
}