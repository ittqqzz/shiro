package com.tqz.shiro.dao;

import org.springframework.stereotype.Component;

@Component
public interface UserRoleAuthDao {

    void delRole(Integer user_id, Integer role_id);

    void delAuth(Integer role_id, Integer auth_id);

    void saveUserRole(Integer user_id, Integer role_id);

    void saveRoleAuth(Integer role_id, Integer auth_id);
}
