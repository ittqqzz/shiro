package com.tqz.shiro.dao;

import com.tqz.shiro.entity.Auth;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface AuthDao {

    List<Auth> queryAuthList();

    void deleteOneAuthById(Integer id);
}
