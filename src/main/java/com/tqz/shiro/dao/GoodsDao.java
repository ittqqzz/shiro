package com.tqz.shiro.dao;

import com.tqz.shiro.entity.Goods;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface GoodsDao {

    void saveOneGoods(Goods goods);

    void delOneGoodsById(Integer id);

    void updateOneGoodsById(Goods goods);

    Goods queryOneGoodsByName(String name);

    List<Goods> queryAllGoods();
}
