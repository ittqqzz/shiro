package com.tqz.shiro.dao;

import com.tqz.shiro.entity.Role;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface RoleDao {

    List<Role> queryRoleList();

    void deleteOneRoleById(Integer id);
}
