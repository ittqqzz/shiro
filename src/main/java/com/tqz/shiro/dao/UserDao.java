package com.tqz.shiro.dao;

import com.tqz.shiro.entity.Auth;
import com.tqz.shiro.entity.Role;
import com.tqz.shiro.entity.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserDao {

    List<User> queryAllUsers();

    User queryUserByUsername(String username);

    User queryUserById(Integer id);

    Integer queryRoleIdByUserId(Integer id);// user_role

    Role queryRoleByRoleId(Integer id);

    List<Integer> queryAuthIdByRoleId(Integer id);// role_auth

    Auth queryAuthByAuthId(Integer id);

    void saveOneUser(User user);

    void delOneUserById(Integer id);
}
