package com.tqz.shiro.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class Auth implements Serializable {

    public Integer id;

    public String auth_name;
}
