package com.tqz.shiro.web;

import com.tqz.shiro.entity.Auth;
import com.tqz.shiro.entity.Role;
import com.tqz.shiro.entity.User;
import com.tqz.shiro.service.*;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.tqz.shiro.utils.QueryUserNameUtils;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private AuthService authService;

    @Autowired
    private UserRoleAuthService userRoleAuthService;

    @RequestMapping("/queryAllUser")
    @RequiresPermissions("user:admin")
    public String queryAllUser(Model model) {
        List<User> users = userService.queryUserList();
        model.addAttribute("users", users);
        String username = QueryUserNameUtils.getUserName();
        model.addAttribute("username", username);
        return "/admin/manageUsers";
    }

    @RequestMapping("/delete")
    @RequiresPermissions("user:admin")
    public String delOneUserById(@RequestParam("id") Integer id) {
        userService.delOneUserById(id);
        return "redirect:/admin/queryAllUser";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    @RequiresPermissions("user:admin")
    public String updateOneUserById(@RequestParam("user_id") Integer user_id,
                                    @RequestParam("role_id") Integer role_id,
                                    @RequestParam("auth_id") Integer auth_id) {
        // 1.往user_role里面插入数据
        if (user_id != role_id) {
            userRoleAuthService.saveUserRole(user_id, role_id);
        }
        // 2.往role_auth里面插入数据
        if (role_id != auth_id) {
            userRoleAuthService.saveRoleAuth(role_id, auth_id);
        }
        return "1";
    }

    @RequestMapping("/updateOneUserPage")
    @RequiresPermissions("user:admin")
    public String gotoUpdateOneUserPage(@RequestParam("id") Integer id, Model model) {
        User user = userService.queryUserById(id);
        model.addAttribute("user", user);
        List<Role> roleList = roleService.queryRoleList();
        model.addAttribute("roleList", roleList);
        List<Auth> authList = authService.queryAuthList();
        model.addAttribute("authList", authList);
        String username = QueryUserNameUtils.getUserName();
        model.addAttribute("username", username);
        return "/admin/updateOneUser";
    }

    @RequestMapping("/delete/role")
    @ResponseBody
    @RequiresPermissions("user:admin")
    public String delRole(@RequestParam("user_id") Integer user_id
            , @RequestParam("role_id") Integer role_id) {
        userRoleAuthService.delRole(user_id, role_id);
        return "1";
    }

    @RequestMapping("/delete/auth")
    @ResponseBody
    @RequiresPermissions("user:admin")
    public String delAuth(@RequestParam("auth_id") Integer auth_id
            , @RequestParam("role_id") Integer role_id) {
        userRoleAuthService.delAuth(role_id, auth_id);
        return "1";
    }

}
