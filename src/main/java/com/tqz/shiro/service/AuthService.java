package com.tqz.shiro.service;

import com.tqz.shiro.entity.Auth;

import java.util.List;

public interface AuthService {

    List<Auth> queryAuthList();

    void deleteOneAuthById(Integer id);
}
