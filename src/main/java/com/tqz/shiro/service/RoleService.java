package com.tqz.shiro.service;

import com.tqz.shiro.entity.Role;

import java.util.List;

public interface RoleService {

    List<Role> queryRoleList();

    void deleteOneRoleById(Integer id);
}
