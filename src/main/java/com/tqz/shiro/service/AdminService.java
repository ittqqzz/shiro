package com.tqz.shiro.service;

import com.tqz.shiro.entity.Auth;
import com.tqz.shiro.entity.Role;

import java.util.List;

public interface AdminService {

    List<Role> queryRoleList();

    void deleteOneRoleById(Integer id);

    List<Auth> queryAuthList();

    void deleteOneAuthById(Integer id);
}
