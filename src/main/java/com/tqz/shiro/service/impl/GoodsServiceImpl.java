package com.tqz.shiro.service.impl;

import com.tqz.shiro.dao.GoodsDao;
import com.tqz.shiro.entity.Goods;
import com.tqz.shiro.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigurationPackage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;

    @Override
    public void saveOneGoods(Goods goods) {
        goodsDao.saveOneGoods(goods);
    }

    @Override
    public void delOneGoodsById(Integer id) {
        goodsDao.delOneGoodsById(id);
    }

    @Override
    public void updateOneGoodsById(Goods goods) {
        goodsDao.updateOneGoodsById(goods);
    }

    @Override
    public Goods queryOneGoodsByName(String name) {
        return goodsDao.queryOneGoodsByName(name);
    }

    @Override
    public List<Goods> queryAllGoods() {
        return goodsDao.queryAllGoods();
    }
}
