package com.tqz.shiro.service.impl;

import com.tqz.shiro.dao.AuthDao;
import com.tqz.shiro.dao.RoleDao;
import com.tqz.shiro.entity.Auth;
import com.tqz.shiro.entity.Role;
import com.tqz.shiro.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class AdminServiceImpl implements AdminService {

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private AuthDao authDao;

    @Override
    public List<Role> queryRoleList() {
        return roleDao.queryRoleList();
    }

    @Override
    public void deleteOneRoleById(Integer id) {
        roleDao.deleteOneRoleById(id);
    }

    @Override
    public List<Auth> queryAuthList() {
        return authDao.queryAuthList();
    }

    @Override
    public void deleteOneAuthById(Integer id) {
        authDao.deleteOneAuthById(id);
    }
}
