package com.tqz.shiro.service.impl;

import com.tqz.shiro.dao.AuthDao;
import com.tqz.shiro.entity.Auth;
import com.tqz.shiro.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AuthServiceImpl implements AuthService {

    @Autowired
    private AuthDao authDao;

    @Override
    public List<Auth> queryAuthList() {
        return authDao.queryAuthList();
    }

    @Override
    public void deleteOneAuthById(Integer id) {
        authDao.deleteOneAuthById(id);
    }
}
