package com.tqz.shiro.service.impl;

import com.tqz.shiro.dao.RoleDao;
import com.tqz.shiro.entity.Role;
import com.tqz.shiro.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public List<Role> queryRoleList() {
        return roleDao.queryRoleList();
    }

    @Override
    public void deleteOneRoleById(Integer id) {
        roleDao.deleteOneRoleById(id);
    }
}
