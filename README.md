# SpringBoot + shiro + mybatis + redis实现RBAC

* 已经完成的功能
  * 登录认证
  * 授权
  * 对要求权限的资源拦截
  * 使用redis缓存记住我的功能
  * 记住我后，服务端重启，session不丢失，刷新网页可以继续浏览
  * 记住我后，redis重启，session不丢失，刷新网页可以继续浏览
  * 退出登录后，清空redis里面缓存的相应数据
* 待开发的功能
  * 实时监控登录用户，踢出在线用户，使其登录状态失效
  * 不可多处登录，仅允许一处登录
  * 单点登录

[我的知乎](https://www.zhihu.com/people/tqz520/posts)